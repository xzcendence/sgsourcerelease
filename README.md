# SGSourceRelease

Source release of SWAPSHOT, including plugins
<br>
*No content is provided, this includes all blueprint classes and other non-native classes

## Some files of importance

Native base classes

[PlayerPawn_Atlas_Base.cpp](Source/AthenaBaseGame/Private/Actors/Pawns/PlayerPawn_Atlas_Base.cpp) - Native base player pawn class
<br>
[Weapon_Atlas_Base.cpp](Source/AthenaBaseGame/Private/Actors/Gameplay/Weapons/Weapon_Atlas_Base.cpp) - Native base weapon class
<br>
[Atlas_Pickup_Master](Source/AthenaBaseGame/Private/Actors/Gameplay/Pickups/Atlas_Pickup_Master.cpp) - Native base pickup class
<br>
[HomingProjectile.cpp](Source/AthenaBaseGame/Private/Actors/Gameplay/Weapons/HomingProjectile.cpp) - Native homing projectile actor class
<br>
[AtlasSwapGameMode.cpp](Source/AthenaBaseGame/Private/Actors/Info/AtlasSwapGameMode.cpp) - Native base game mode class
<br>
[AtlasSwapGameState.cpp](Source/AthenaBaseGame/Private/Actors/Info/AtlasSwapGameState.cpp) - Native base game state class
<br>
[AtlasWorldSettings.cpp](Source/AthenaBaseGame/Private/Actors/Info/AtlasWorldSettings.cpp) - Native world settings class
<br>
[GameVersionSubsystem.cpp](Plugins/GameVersion/Source/GameVersion/Private/GameVersionSubsystem.cpp) - GameInstanceSubsystem used for runtime version checks and management on the client and backend
<br>
[AtlasTypes.h](Source/AthenaBaseGame/Public/AtlasTypes.h) - Header containing game-specific types

### Source Tree
```
Source
├── AthenaBaseGame
│   ├── AthenaBaseGame.Build.cs
│   ├── AthenaBaseGame.cpp
│   ├── AthenaBaseGame.h
│   ├── Private
│   │   ├── Actors
│   │   │   ├── Gameplay
│   │   │   │   ├── JumpPad_Atlas.cpp
│   │   │   │   ├── Pickups
│   │   │   │   │   └── Atlas_Pickup_Master.cpp
│   │   │   │   └── Weapons
│   │   │   │       ├── HomingProjectile.cpp
│   │   │   │       └── Weapon_Atlas_Base.cpp
│   │   │   ├── Info
│   │   │   │   ├── AtlasPlayerState.cpp
│   │   │   │   ├── AtlasSwapGameMode.cpp
│   │   │   │   ├── AtlasSwapGameState.cpp
│   │   │   │   ├── AtlasWorldManager.cpp
│   │   │   │   ├── AtlasWorldSettings.cpp
│   │   │   │   └── Player
│   │   │   │       ├── AtlasPlayerDamageManager.cpp
│   │   │   │       └── AtlasPlayerDamageManager.h
│   │   │   └── Pawns
│   │   │       ├── PlayerPawn_Atlas_Base.cpp
│   │   │       └── SpectatorPawn_Atlas_Base.cpp
│   │   ├── AtlasScopedRifle.cpp
│   │   ├── AtlasTypes.cpp
│   │   ├── Objects
│   │   │   ├── AtlasCameraComponent.cpp
│   │   │   └── Projectiles
│   │   │       ├── AtlasProjectileMovementComponent.cpp
│   │   │       └── PSProjectile_Atlas.cpp
│   │   ├── SwapPlayerController.cpp
│   │   └── Utilities
│   │       ├── Audio
│   │       │   ├── Atlas_WeaponAudioComponent.cpp
│   │       │   └── SoundNodeLocalPlayer.cpp
│   │       ├── Game
│   │       │   ├── AtlasGameInstance.cpp
│   │       │   └── AtlasGameInstance.h
│   │       ├── HUD
│   │       │   └── DebugHUDAtlas.cpp
│   │       ├── Helios
│   │       │   └── HeliosPlaylistManager.cpp
│   │       ├── HelpfulBlueprintFunctions.cpp
│   │       └── Replication
│   │           ├── AtlasReplicationGraph.cpp
│   │           └── AtlasReplicationGraph.h
│   └── Public
│       ├── Actors
│       │   ├── Gameplay
│       │   │   ├── JumpPad_Atlas.h
│       │   │   ├── Pickups
│       │   │   │   └── Atlas_Pickup_Master.h
│       │   │   └── Weapons
│       │   │       ├── HomingProjectile.h
│       │   │       └── Weapon_Atlas_Base.h
│       │   ├── Info
│       │   │   ├── AtlasPlayerState.h
│       │   │   ├── AtlasSwapGameMode.h
│       │   │   ├── AtlasSwapGameState.h
│       │   │   ├── AtlasWorldManager.h
│       │   │   └── AtlasWorldSettings.h
│       │   └── Pawns
│       │       ├── PlayerPawn_Atlas_Base.h
│       │       └── SpectatorPawn_Atlas_Base.h
│       ├── AtlasScopedRifle.h
│       ├── AtlasTypes.h
│       ├── Objects
│       │   ├── AtlasCameraComponent.h
│       │   └── Projectiles
│       │       ├── AtlasProjectileMovementComponent.h
│       │       └── PSProjectile_Atlas.h
│       ├── SwapPlayerController.h
│       └── Utilities
│           ├── Audio
│           │   ├── Atlas_WeaponAudioComponent.h
│           │   └── SoundNodeLocalPlayer.h
│           ├── HUD
│           │   └── DebugHUDAtlas.h
│           ├── Helios
│           │   └── HeliosPlaylistManager.h
│           └── HelpfulBlueprintFunctions.h
├── AthenaBaseGame.Target.cs
└── AthenaBaseGameEditor.Target.cs
```