// Any content from AthenaBaseGame is not to be reproduced or copied without permission from the owning author, redistribution is strictly proprietary, prohibited and confidential

#pragma once

#include "CoreMinimal.h"
#include "ReplicationGraph.h"
#include "AtlasReplicationGraph.generated.h"

/**
 * 
 */
UCLASS()
class UAtlasReplicationGraph : public UReplicationGraph
{
	GENERATED_BODY()
	
};
