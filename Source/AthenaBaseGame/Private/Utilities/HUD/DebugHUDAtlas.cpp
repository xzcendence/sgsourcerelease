// Any content from AthenaBaseGame is not to be reproduced or copied without permission from the owning author, redistribution is strictly proprietary, prohibited and confidential


#include "DebugHUDAtlas.h"

FString ADebugHUDAtlas::GetNetGUIDForActorAsString(AActor* Actor)
{
	if (Actor && Actor->GetNetDriver())
	{
		FString NetGUID = (GetNetDriver()->GuidCache->GetNetGUID(Actor)).ToString();
		return NetGUID;
	}
	return FString();
}
	
