// Any content from AthenaBaseGame is not to be reproduced or copied without permission from the owning author, redistribution is strictly proprietary, prohibited and confidential

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "AtlasGameInstance.generated.h"

/**
 * 
 */
UCLASS()
class UAtlasGameInstance : public UGameInstance
{
	GENERATED_BODY()
	
};
