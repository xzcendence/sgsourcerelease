// Any content from AthenaBaseGame is not to be reproduced or copied without permission from the owning author, redistribution is strictly proprietary, prohibited and confidential


#include "HelpfulBlueprintFunctions.h"
#include "AthenaBaseGame.h"

void UHelpfulBlueprintFunctions::DrawDebugStringAtLocation(UObject* WorldContextObject, const FVector TextLocation, const FString& Text, class AActor* TestBaseActor, FLinearColor TextColor, float Duration, bool bDrawShadow, float FontScale)
{
	if (UWorld* World = GEngine->GetWorldFromContextObject(WorldContextObject, EGetWorldErrorMode::LogAndReturnNull))
	{
		DrawDebugString(World, TextLocation, Text, TestBaseActor, TextColor.ToFColor(true), Duration, bDrawShadow, FontScale);
	}
}

void UHelpfulBlueprintFunctions::PrintStringToLog(bool bLogVerbose, FString LogString)
{
	if (bLogVerbose == false)
	{
		UE_LOG(LogAtlas, Log, TEXT("%s"), *LogString);
	}
	else
	{
		UE_LOG(LogAtlasVerbose, Verbose, TEXT("%s"), *LogString)
	}
}

bool UHelpfulBlueprintFunctions::IsInEditor()
{
	return(GIsEditor);	
}