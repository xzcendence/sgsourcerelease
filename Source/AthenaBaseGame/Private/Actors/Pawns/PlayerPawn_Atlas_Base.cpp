


#include "PlayerPawn_Atlas_Base.h"
#include "UnrealNetwork.h"

// Sets default values
APlayerPawn_Atlas_Base::APlayerPawn_Atlas_Base()
{
 	// Set this character to call Tick() every frame.
	PrimaryActorTick.bCanEverTick = true;
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = true;
	bUseControllerRotationRoll = false;
	GetCharacterMovement()->bOrientRotationToMovement = false;
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.0f, 0.0f);
	GetCharacterMovement()->JumpZVelocity = 600.0f;
	GetCharacterMovement()->AirControl = 0.2f;
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = 350.0f;
	CameraBoom->bUsePawnControlRotation = true;
	CameraM = CreateDefaultSubobject<UAtlasCameraComponent>(TEXT("CameraM"));
	CameraM->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	CameraM->bUsePawnControlRotation = false;
	MainAmmo = 214000;
	PlayerMainColor = FColor::Orange;
}
//Called whenever bIsJumpDown changes
void APlayerPawn_Atlas_Base::OnRep_bIsJumpDown_Implementation()
{
	//Fires a blueprint native event (when called without the "_Implementation" part
}

UWorld* APlayerPawn_Atlas_Base::GetWorldObject()
{
	return GetWorld();
}

// Called when the game starts or when spawned
void APlayerPawn_Atlas_Base::BeginPlay()
{
	Super::BeginPlay();
	
}
// Called every frame
void APlayerPawn_Atlas_Base::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}
// Called to bind functionality to input
void APlayerPawn_Atlas_Base::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);
	PlayerInputComponent->BindAction("Crouch", IE_Pressed, this, &APlayerPawn_Atlas_Base::StartCrouching);
	PlayerInputComponent->BindAction("Crouch", IE_Released, this, &APlayerPawn_Atlas_Base::StopCrouching);
	PlayerInputComponent->BindAxis("MoveForward", this, &APlayerPawn_Atlas_Base::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &APlayerPawn_Atlas_Base::MoveRight);

}
//Move forward and backward axis control
void APlayerPawn_Atlas_Base::MoveForward(float Axis)
{
	FRotator Rotation = Controller->GetControlRotation();
	FRotator YawRotation(0.0f, Rotation.Yaw, 0.0f);

	FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
	AddMovementInput(Direction, Axis);
}
//Move left and right axis control
void APlayerPawn_Atlas_Base::MoveRight(float Axis)
{
	FRotator Rotation = Controller->GetControlRotation();
	FRotator YawRotation(0.0f, Rotation.Yaw, 0.0f);

	FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
	AddMovementInput(Direction, Axis);
}
//Calls ACharacter::Crouch only if pawn is not in air/jumping/falling
void APlayerPawn_Atlas_Base::StartCrouching()
{
	if ((GetCharacterMovement()->MovementMode == EMovementMode::MOVE_Falling) && (bIsADSDown == false))
	{
		
	}
	else
	{
		Crouch(true);
	}
	
}

void APlayerPawn_Atlas_Base::WeaponNearby_Implementation(AWeapon_Atlas_Base* Weapon, bool ComingOrLeaving)
{

}
//Calls ACharacter::UnCrouch;
void APlayerPawn_Atlas_Base::StopCrouching()
{
	UnCrouch(true);
}
void APlayerPawn_Atlas_Base::KillMinimalWorldOnly_Implementation()
{
}

void APlayerPawn_Atlas_Base::FellOutOfWorld(const class UDamageType& dmgType)
{
	KillMinimalWorldOnly();
}
//Replicates Variables via DOREPLIFETIME
void APlayerPawn_Atlas_Base::GetLifetimeReplicatedProps(TArray< FLifetimeProperty >& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(APlayerPawn_Atlas_Base, bIsJumpDown);
	DOREPLIFETIME(APlayerPawn_Atlas_Base, EquippedWeaponClass);
	DOREPLIFETIME(APlayerPawn_Atlas_Base, bIsADSDown);
	DOREPLIFETIME(APlayerPawn_Atlas_Base, EquippedWeapon);
	DOREPLIFETIME(APlayerPawn_Atlas_Base, NearbyWeapon);
	DOREPLIFETIME(APlayerPawn_Atlas_Base, WeaponTraceResult);
	DOREPLIFETIME(APlayerPawn_Atlas_Base, MainAmmo);
	DOREPLIFETIME(APlayerPawn_Atlas_Base, LauncherAmmo);
	DOREPLIFETIME(APlayerPawn_Atlas_Base, PlayerMainColor);
	
}
