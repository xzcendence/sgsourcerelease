// Any content from AthenaBaseGame is not to be reproduced or copied without permission from the owning author, redistribution is strictly proprietary, prohibited and confidential


#include "JumpPad_Atlas.h"
#include "Components/ArrowComponent.h"
#include "PlayerPawn_Atlas_Base.h"

// Sets default values
AJumpPad_Atlas::AJumpPad_Atlas()
{

	PrimaryActorTick.bCanEverTick = true;
	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));
	StaticMesh->OnComponentBeginOverlap.AddDynamic(this, &AJumpPad_Atlas::OnStaticMeshOverlap);
	ArrowPoint = CreateDefaultSubobject<UArrowComponent>(TEXT("ArrowPoint"));
	ArrowPoint->SetupAttachment(StaticMesh);
	ArrowPoint->SetArrowColor(FLinearColor(1.f, 0.21f, 0.f, 1.f));
	ArrowPoint->ArrowSize = 2.5f;
	DirectionPointParticle = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("DirectionPointParticle"));
	DirectionPointParticle->SetupAttachment(ArrowPoint);
	bReplicates = true;

}
/*
Multicast RPC for launching the player 
*/
void AJumpPad_Atlas::LaunchCharacter_Multi_Implementation(ACharacter* LaunchTarget, FVector LaunchVelocity)
{
	bool bPlayerHasNegativeZVelocity = LaunchTarget->GetVelocity().Z < 0; //Is the player's Z velocity less than 0 (is it negative)
	LaunchTarget->LaunchCharacter(LaunchVelocity, true, bPlayerHasNegativeZVelocity);
}

void AJumpPad_Atlas::OnStaticMeshOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (GetLocalRole() == ROLE_Authority)
	{

		if (APlayerPawn_Atlas_Base* Character = Cast<APlayerPawn_Atlas_Base>(OtherActor))
		{
			LaunchCharacter_Multi(Cast<ACharacter>(OtherActor), ArrowPoint->GetForwardVector() * LaunchVelocityMultiplier * 100);
		}
	}
}

// Called when the game starts or when spawned
void AJumpPad_Atlas::BeginPlay()
{
	Super::BeginPlay();
}


// Called every frame
void AJumpPad_Atlas::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

