// Any content from AthenaBaseGame is not to be reproduced or copied without permission from the owning author, redistribution is strictly proprietary, prohibited and confidential


#include "HomingProjectile.h"
#include "PlayerPawn_Atlas_Base.h"
#include "Math/Rotator.h"
#include "UnrealNetwork.h"
#include "Engine.h"

// Sets default values
AHomingProjectile::AHomingProjectile()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	bReplicates = true;
	SetReplicateMovement(true);
	ProjectileMovement = CreateDefaultSubobject<UAtlasProjectileMovementComponent>(TEXT("ProjectileMovement"));
	ProjectileMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));
	ProjectileCollision = CreateDefaultSubobject<UCapsuleComponent>(TEXT("ProjectileCollision"));
	HeadingParticle = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("HeadingParticle"));
	TrailParticle = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("TrailParicle"));
	InAirSouund = CreateDefaultSubobject<UAudioComponent>(TEXT("InAirSound"));
	ProjectileCollision->OnComponentHit.AddDynamic(this, &AHomingProjectile::OnProjectileBeginOverlap);
	SetRootComponent(ProjectileCollision);
	ProjectileMesh->SetupAttachment(RootComponent);
	HeadingParticle->SetupAttachment(ProjectileMesh);
	TrailParticle->SetupAttachment(ProjectileMesh);
	InAirSouund->SetupAttachment(RootComponent);
	ProjectileMovement->bIsHomingProjectile = true;
	ProjectileMovement->SetIsReplicated(true);
	ProjectileMovement->UpdatedComponent = ProjectileCollision;
	ProjectileMovement->InitialSpeed = 2000;
	ProjectileMovement->ProjectileGravityScale = 0.f;
	ProjectileMovement->MaxSpeed = MaxSpeed;
	ProjectileMovement->bShouldBounce = false;
	ProjectileMovement->bRotationFollowsVelocity = true;
	ProjectileMovement->HomingAccelerationMagnitude = HomingAccelerationMagnitude;
	ProjectileMovement->bShouldBounce = false;
}

void AHomingProjectile::OnProjectileBeginOverlap(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Emerald, FString::Printf(TEXT("Projectile Was Hit")));
	if (HasAuthority())
	{
		if (OtherActor != GetInstigator() && OtherActor != InstigatingWeapon && IsValid(InstigatingWeapon))
		{
			NetMulticast_ProjectileHitEffects(GetActorLocation());
			if (IsValid(OtherActor))
			{
				InstigatingWeapon->OnProjectileHit(GetActorLocation(), OtherActor, GetInstigator());
			}
			else
			{
				InstigatingWeapon->OnProjectileHit(GetActorLocation(), nullptr, GetInstigator());
			}
			Destroy();
		}
	}
}

void AHomingProjectile::SetHomingTarget(USceneComponent* HomingTargetComp, float DelayToStartHoming)
{
	if (GetLocalRole() == ROLE_Authority)
	{
		ProjectileMovement->HomingTargetComponent = HomingTargetComp;
		FTimerHandle HomingVelocityDelay;
		GetWorldTimerManager().SetTimer(HomingVelocityDelay, this, &AHomingProjectile::SetVelocityForHomingDelay, DelayToStartHoming, false);
		FTimerHandle HomingLifetimeTimer;
		GetWorldTimerManager().SetTimer(HomingLifetimeTimer, this, &AHomingProjectile::ForceExplode, HomingLifetime, false);
	}
}

void AHomingProjectile::ForceExplode()
{
	if (GetLocalRole() == ROLE_Authority && IsValid(InstigatingWeapon))
	{
		NetMulticast_ProjectileHitEffects(GetActorLocation());
		Destroy();
		InstigatingWeapon->OnProjectileHit(GetActorLocation(), nullptr, GetInstigator());
	}
}

void AHomingProjectile::SetVelocityForHomingDelay()
{
	if (GetLocalRole() == ROLE_Authority)
	{
		ProjectileMovement->Velocity = FVector::ZeroVector;
		ProjectileMovement->HomingAccelerationMagnitude = 4114.f;
		FTimerHandle FullLifetimeTimer;
		GetWorldTimerManager().SetTimer(FullLifetimeTimer, this, &AHomingProjectile::ForceExplode, FullLifetime, false);
	}
}

void AHomingProjectile::NetMulticast_ProjectileHitEffects_Implementation(const FVector HitLocation)
{
	UGameplayStatics::SpawnEmitterAtLocation(this, ProjectileImpactParticle, HitLocation, FRotator::ZeroRotator, FVector(3), true, EPSCPoolMethod::None, true);
	UGameplayStatics::PlaySoundAtLocation(this, ProjectileImpactSound, HitLocation, FRotator::ZeroRotator, 1.5f, 1.f, 0.f, ProjectileImpactSoundAttenuation, nullptr, this);
}

// Called when the game starts or when spawned
void AHomingProjectile::BeginPlay()
{
	Super::BeginPlay();
	if (GetLocalRole() == ROLE_Authority)
	{
		ProjectileMovement->HomingAccelerationMagnitude = HomingAccelerationMagnitude;
		ProjectileMovement->MaxSpeed = MaxSpeed;
		ProjectileMovement->InitialSpeed = InitialSpeed;
	}
}

// Called every frame
void AHomingProjectile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AHomingProjectile::GetLifetimeReplicatedProps(TArray< FLifetimeProperty >& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(AHomingProjectile, HomingTarget);
	DOREPLIFETIME(AHomingProjectile, InstigatingWeapon);
}

