// Any content from AthenaBaseGame is not to be reproduced or copied without permission from the owning author, redistribution is strictly proprietary, prohibited and confidential


#include "Weapon_Atlas_Base.h"
#include "Engine.h"
#include "UnrealNetwork.h"
#include "PlayerPawn_Atlas_Base.h"
#include "AtlasSwapGameMode.h"
#include "AthenaBaseGame.h"

// Sets default values
AWeapon_Atlas_Base::AWeapon_Atlas_Base()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	WeaponMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("WeaponMesh"));
	RootComponent = WeaponMesh;
	PickupBounds = CreateDefaultSubobject<USphereComponent>(TEXT("PickupBounds"));
	PickupBounds->SetupAttachment(WeaponMesh);
	MuzzleLocation = CreateDefaultSubobject<USphereComponent>(TEXT("MuzzleLocation"));
	MuzzleLocation->SetupAttachment(WeaponMesh);
	NetUpdateFrequency = 60.f;
	PickupBounds->OnComponentBeginOverlap.AddDynamic(this, &AWeapon_Atlas_Base::OnPlayerEnterPickupBounds);
	PickupBounds->OnComponentEndOverlap.AddDynamic(this, &AWeapon_Atlas_Base::OnPlayerLeavePickupBounds);
	SetReplicateMovement(true);
	bReplicates = true;
	CurrentClipAmmo = WeaponAttributes.MaxClipAmmo;
}

// Called when the game starts or when spawned
void AWeapon_Atlas_Base::BeginPlay()
{
	Super::BeginPlay();
	if (GetLocalRole() == ROLE_Authority)
	{
		RegisterToWorldWeaponPool();
		AssignWeaponGUID();
	}
}

void AWeapon_Atlas_Base::Destroyed()
{
	DeregisterFromWorldWeaponPool();
}

void AWeapon_Atlas_Base::OnPlayerLeavePickupBounds_Implementation(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (APlayerPawn_Atlas_Base* PlayerPawn = Cast<APlayerPawn_Atlas_Base>(OtherActor))
	{
		//Tells the NearbyPlayer what weapon they are near.
		PlayerPawn->NearbyWeapon = nullptr;
		if (PlayerPawn->IsLocallyControlled())
		{
			PlayerPawn->WeaponNearby(this, false);
			WeaponMesh->SetRenderCustomDepth(false);
		}
	}
}

void AWeapon_Atlas_Base::DetachFromOwner()
{
	DetachFromActor(FDetachmentTransformRules::KeepWorldTransform);
}

bool AWeapon_Atlas_Base::RegisterToWorldWeaponPool()
{
	if (AAtlasSwapGameMode* GameMode = Cast<AAtlasSwapGameMode>(UGameplayStatics::GetGameMode(GetWorld())))
	{
		const TArray<AWeapon_Atlas_Base*> WorldWeaponPoolArray = GameMode->WorldWeaponPool;
		if (WorldWeaponPoolArray.Contains(this))
		{
			UE_LOG(LogAtlasVerbose, Log, TEXT("Weapon %s is already registered to world weapon pool"), *(GetName()));
			//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Orange, FString::Printf(TEXT("Weapon %s was registered to world weapon pool"), *(GetName())));
			return false;
		}
		GameMode->WorldWeaponPool.Push(this);
		UE_LOG(LogAtlasVerbose, Log, TEXT("Weapon %s was successfully registered to the world weapon pool"), *(GetName()));
		return true;
	}
	return false;
}

bool AWeapon_Atlas_Base::DeregisterFromWorldWeaponPool()
{
	if (AAtlasSwapGameMode* GameMode = Cast<AAtlasSwapGameMode>(UGameplayStatics::GetGameMode(GetWorld())))
	{
		GameMode->WorldWeaponPool.Remove(this);
		return true;
	}
	return false;
}

void AWeapon_Atlas_Base::AssignWeaponGUID()
{
	WeaponGUID = FGuid::NewGuid();
	UE_LOG(LogAtlasVerbose, Log, TEXT("Weapon %s was assigned GUID %s"), *(GetName()), *(WeaponGUID.ToString()));
}

//Fires whenever a player is nearby
void AWeapon_Atlas_Base::OnPlayerEnterPickupBounds_Implementation(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
    if (bIsInUse)
    {
        return;
    }

    if (APlayerPawn_Atlas_Base* PlayerPawn = Cast<APlayerPawn_Atlas_Base>(OtherActor))
    {
        // Tells the NearbyPlayer what weapon they are near.
        PlayerPawn->NearbyWeapon = this;
        if (PlayerPawn->IsLocallyControlled())
        {
            PlayerPawn->WeaponNearby(this, true);
            WeaponMesh->SetRenderCustomDepth(true);
        }
    }
}

void AWeapon_Atlas_Base::Equip_Implementation(){}

void AWeapon_Atlas_Base::OnProjectileHit_Implementation(const FVector HitLocation, AActor* HitActor, APawn* InstigatingPawn){}

// Called every frame
void AWeapon_Atlas_Base::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

bool AWeapon_Atlas_Base::SRVFire_Validate()
{
	return true;
	//This is just for testing, there should be some additional parameters dictating this later
}

bool AWeapon_Atlas_Base::SRVFireRelease_Validate()
{
	return true;
	//This is just for testing, there should be some additional parameters dictating this later
}

void AWeapon_Atlas_Base::SRVFire_Implementation()
{
	Fire();
}

void AWeapon_Atlas_Base::SRVFireRelease_Implementation()
{
	FireRelease();
}

void AWeapon_Atlas_Base::Fire_Implementation(){}

void AWeapon_Atlas_Base::FireRelease_Implementation(){}

//Setup replicated variables
void AWeapon_Atlas_Base::GetLifetimeReplicatedProps(TArray< FLifetimeProperty >& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AWeapon_Atlas_Base, bIsInUse);
	DOREPLIFETIME(AWeapon_Atlas_Base, bCanBePickedUp);
	DOREPLIFETIME(AWeapon_Atlas_Base, CurrentClipAmmo);
	DOREPLIFETIME(AWeapon_Atlas_Base, WeaponAttributes)
	DOREPLIFETIME(AWeapon_Atlas_Base, GunmanOwner);
	DOREPLIFETIME(AWeapon_Atlas_Base, bCanDealDamage);
	DOREPLIFETIME(AWeapon_Atlas_Base, bHasInfiniteAmmo);
	DOREPLIFETIME(AWeapon_Atlas_Base, WeaponGUID);
}

