// Any content from AthenaBaseGame is not to be reproduced or copied without permission from the owning author, redistribution is strictly proprietary, prohibited and confidential


#include "Atlas_Pickup_Master.h"

// Sets default values
AAtlas_Pickup_Master::AAtlas_Pickup_Master()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	NetUpdateFrequency = 10.f;
	PickupBounds = CreateDefaultSubobject<UCapsuleComponent>(TEXT("PickupBounds"));
	RootComponent = PickupBounds;
	ItemParticle = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("ItemParticle"));
	ItemParticle->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void AAtlas_Pickup_Master::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AAtlas_Pickup_Master::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

