// Any content from AthenaBaseGame is not to be reproduced or copied without permission from the owning author, redistribution is strictly proprietary, prohibited and confidential


#include "AtlasSwapGameMode.h"
#include "Weapon_Atlas_Base.h"
#include "EngineUtils.h"
#include "AthenaBaseGame.h"

//Don't use these they succ
void AAtlasSwapGameMode::BeginPlay() 
{
	RefreshWorldWeaponPool();
	Super::BeginPlay();
}

APawn* AAtlasSwapGameMode::SpawnDefaultPawnAtTransform_Implementation(AController* NewPlayer, const FTransform& SpawnTransform)
{
	UClass* PawnClass = GetDefaultPawnClassForController(NewPlayer);
	//Now we'll use SpawnActorDeferred instead so we can initialize some variables before finishing the spawn
	APawn* ResultPawn = GetWorld()->SpawnActorDeferred<APawn>(PawnClass, SpawnTransform, NewPlayer, GetInstigator(), ESpawnActorCollisionHandlingMethod::AlwaysSpawn);
	if (!ResultPawn)
	{
		UE_LOG(LogGameMode, Warning, TEXT("SpawnDefaultPawnAtTransform: Couldn't spawn Pawn of type %s at %s"), *GetNameSafe(PawnClass), *SpawnTransform.ToHumanReadableString());
	}
	APlayerPawn_Atlas_Base* AtlasPawn = Cast<APlayerPawn_Atlas_Base>(ResultPawn);
	if (AtlasPawn)
	{
		//For now all that we're going to do differently here is spawn the player with a random color, later on we'll use the player's chosen color from the PlayerState (or PlayerController, we'll see)
		AtlasPawn->PlayerMainColor = FLinearColor::MakeRandomColor();
	}
	//Finish spawning the pawn
	UGameplayStatics::FinishSpawningActor(ResultPawn, SpawnTransform);
	//Log that we've successfully spawned the pawn
	UE_LOG(LogGameMode, Verbose, TEXT("SpawnDefaultPawnAtTransform: Spawned Pawn %s of type %s at %s"), *GetNameSafe(AtlasPawn), *GetNameSafe(PawnClass), *SpawnTransform.ToHumanReadableString());
	return ResultPawn;
}

bool AAtlasSwapGameMode::SetEnableInfiniteAmmoAllWeapons(const bool NewEnable)
{
	for (int i = 0; i < WorldWeaponPool.Num(); i++) 
	{
		WorldWeaponPool[i]->bHasInfiniteAmmo = NewEnable;
	}
	return true;
}

void AAtlasSwapGameMode::SetCanDealDamageAllWeapons(const bool NewEnable)
{
	for (int i = 0; i < WorldWeaponPool.Num(); i++)
	{
		WorldWeaponPool[i]->bCanDealDamage = NewEnable;
	}
}

void AAtlasSwapGameMode::RestartPlayerAtlas(AController* NewPlayer, APawn* OldPlayerPawn)
{
	OldPlayerPawn->Destroy();
	RestartPlayer(NewPlayer);
}

void AAtlasSwapGameMode::RefreshWorldWeaponPool()
{
	WorldWeaponPool.Empty();
	for (TActorIterator<AWeapon_Atlas_Base> It(GetWorld()); It; ++It)
	{
		WorldWeaponPool.Push(*It);
	}
	UE_LOG(LogAtlasVerbose, Log, TEXT("Weapons got added to the world weapon pool ig"));
}

