// Any content from AthenaBaseGame is not to be reproduced or copied without permission from the owning author, redistribution is strictly proprietary, prohibited and confidential


#include "AtlasPlayerState.h"
#include "UnrealNetwork.h"

void AAtlasPlayerState::GetLifetimeReplicatedProps(TArray< FLifetimeProperty >& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AAtlasPlayerState, PlayerMatchStat);

}

