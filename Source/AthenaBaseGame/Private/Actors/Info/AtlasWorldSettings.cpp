// Any content from AthenaBaseGame is not to be reproduced or copied without permission from the owning author, redistribution is strictly proprietary, prohibited and confidential


#include "AtlasWorldSettings.h"



AAtlasWorldSettings::AAtlasWorldSettings()
{
	PrimaryActorTick.bCanEverTick = true;
}



AAtlasWorldSettings* AAtlasWorldSettings::GetAtlasWorldSettings(UObject* WorldContextObject)
{
	if (UWorld* World = GEngine->GetWorldFromContextObject(WorldContextObject, EGetWorldErrorMode::LogAndReturnNull))
	{
		if (AAtlasWorldSettings* AtlasWorldSettings = Cast<AAtlasWorldSettings>(World->GetWorldSettings()))
		{
			return AtlasWorldSettings;
		}
	}
	return nullptr;
}

void AAtlasWorldSettings::BeginPlay()
{
	Super::BeginPlay();
}

void AAtlasWorldSettings::Tick(float DeltaTime)
{
}