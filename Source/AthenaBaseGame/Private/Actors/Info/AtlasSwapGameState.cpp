// Any content from AthenaBaseGame is not to be reproduced or copied without permission from the owning author, redistribution is strictly proprietary, prohibited and confidential


#include "Actors/Info/AtlasSwapGameState.h"

AAtlasSwapGameState::AAtlasSwapGameState()
{
	//If we are the server instance, Bind the NetMulticast function to the OnKillFeedEvent delegate
	if (HasAuthority())
	{
		OnKillFeedEvent.AddDynamic(this, &AAtlasSwapGameState::MulticastOnKillFeedEvent);
	}
}

void AAtlasSwapGameState::MulticastOnKillFeedEvent_Implementation(FKillFeedEntryInfo KillFeedEntryInfo)
{
	//Get the PlayerController
	ASwapPlayerController* PlayerController = Cast<ASwapPlayerController>(GEngine->GetFirstLocalPlayerController(GetWorld()));
	//Ensure that the PlayerController is valid, because when this code executes on a dedicated server, there is no PlayerController (thanks for reminding me, VS debugger)
	if (IsValid(PlayerController))
	{
		//Call the AddKillFeedEntry function on the PlayerController
		PlayerController->AddKillFeedEntry(KillFeedEntryInfo);
	}
	//It it also 3 AM and I am going to have an aneurysm. I originally thought the data structure being passed in is the problem, but it was the PlayerController being null.
	//Add debug message to screen
	//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Emerald, FString::Printf(TEXT("OnKillFeedEvent multicast was called")));
}
