// Any content from AthenaBaseGame is not to be reproduced or copied without permission from the owning author, redistribution is strictly proprietary, prohibited and confidential


#include "AtlasPlayerDamageManager.h"

// Sets default values for this component's properties
UAtlasPlayerDamageManager::UAtlasPlayerDamageManager()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UAtlasPlayerDamageManager::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UAtlasPlayerDamageManager::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

