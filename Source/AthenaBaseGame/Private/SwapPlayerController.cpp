// Any content from AthenaBaseGame is not to be reproduced or copied without permission from the owning author, redistribution is strictly proprietary, prohibited and confidential


#include "SwapPlayerController.h"

ASwapPlayerController::ASwapPlayerController()
{
}

void ASwapPlayerController::EnableCheatsDetected_Implementation()
{
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Emerald, FString::Printf(TEXT("EnableCheats was called!")));
}

void ASwapPlayerController::EnableCheats()
{
	EnableCheatsDetected();
}

void ASwapPlayerController::AddKillFeedEntry_Implementation(FKillFeedEntryInfo KillFeedEntryInfo)
{
	
}

FLinearColor ASwapPlayerController::GetPlayerColor() const
{
	return FLinearColor::MakeRandomColor();
}
