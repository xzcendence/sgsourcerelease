// Any content from AthenaBaseGame is not to be reproduced or copied without permission from the owning author, redistribution is strictly proprietary, prohibited and confidential


#include "Objects/AtlasCameraComponent.h"

void UAtlasCameraComponent::AddFieldOfViewOffset(float Offset)
{
	SetFieldOfView((FieldOfView + Offset));
}