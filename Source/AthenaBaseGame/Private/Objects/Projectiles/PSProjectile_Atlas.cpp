// Any content from AthenaBaseGame is not to be reproduced or copied without permission from the owning author, redistribution is strictly proprietary, prohibited and confidential

//PSProjectile stands for Physics-Simulated-Projectile, used for weapons such as snipers and other weapons that need simulated gravity and bullet drop
#include "PSProjectile_Atlas.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "GameFramework/Character.h"
#include "PlayerPawn_Atlas_Base.h"
#include "Engine/Engine.h"
#include "Particles/ParticleSystemComponent.h"
#include "UnrealNetwork.h"

// Sets default values
APSProjectile_Atlas::APSProjectile_Atlas()
{
 	
	bReplicates = true;
	SetReplicateMovement(true);
	ProjectileSpeed = 40000.f;
	GravityDelay = 0.1f;
	GravityScale = 3.5;
	ProjectileCollision = CreateDefaultSubobject<UBoxComponent>(TEXT("ProjectileColision"));
	ProjectileCollision->InitBoxExtent(FVector(2.f));
	ProjectileCollision->BodyInstance.SetCollisionProfileName("Projectile");
	ProjectileCollision->OnComponentHit.AddDynamic(this, &APSProjectile_Atlas::OnProjectileHit);
	SetRootComponent(ProjectileCollision);
	ProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("ProjectileMovement"));
	ProjectileMovement->UpdatedComponent = ProjectileCollision;
	ProjectileMovement->InitialSpeed = ProjectileSpeed;
	ProjectileMovement->bShouldBounce = false;
	ProjectileMovement->ProjectileGravityScale = GravityScale;
	ProjectileMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("ProjectileMesh"));
	ProjectileMesh->SetupAttachment(RootComponent);
	ProjectileParticles = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("ProjectileParticleSystem"));
	ProjectileParticles->SetupAttachment(RootComponent);
	InitialLifeSpan = 3.0f;

}

void APSProjectile_Atlas::OnProjectileHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	if (APlayerPawn_Atlas_Base* Character = Cast<APlayerPawn_Atlas_Base>(OtherActor))
	{
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Cyan, FString::Printf(TEXT("A PlayerPawn Was hit (projectile is talking)")));
	}
	
	//Destroys itself once something is hit
	Destroy();
}

// Called when the game starts or when spawned
void APSProjectile_Atlas::BeginPlay()
{
	Super::BeginPlay();
	
	//After gravity delay, make the bullet drop
	FTimerHandle DummyHandle;
	GetWorldTimerManager().SetTimer(DummyHandle, this, &APSProjectile_Atlas::ApplyGravity, GravityDelay, false);
}


void APSProjectile_Atlas::ApplyGravity()
{
	ProjectileMovement->ProjectileGravityScale = GravityScale;
}

// Called every frame
void APSProjectile_Atlas::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void APSProjectile_Atlas::GetLifetimeReplicatedProps(TArray< FLifetimeProperty >& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(APSProjectile_Atlas, ProjectileSpeed);
	DOREPLIFETIME(APSProjectile_Atlas, GravityDelay);
	DOREPLIFETIME(APSProjectile_Atlas, GravityScale);
	DOREPLIFETIME(APSProjectile_Atlas, InstigatingWeapon);
}

