// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"

DECLARE_LOG_CATEGORY_EXTERN(LogAtlas, Log, All);
DECLARE_LOG_CATEGORY_EXTERN(LogAtlasVerbose, Verbose, All);
