// Any content from AthenaBaseGame is not to be reproduced or copied without permission from the owning author, redistribution is strictly proprietary, prohibited and confidential

#pragma once

#include "CoreMinimal.h"
#include "AtlasTypes.generated.h"

class APlayerState;
class AWeapon_Atlas_Base;

/**
 * Project-Specific types are defined in this header.
 */



/* *****GAMEPLAY-RELATED TYPES***** */

/**
 * Data structure containing the info for a kill feed entry
 */
USTRUCT(BlueprintType)
struct FKillFeedEntryInfo
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		APlayerState* Instigator;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		AWeapon_Atlas_Base* InflictingWeapon;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		APlayerState* Victim;


};

/**
 * Data structure containing the 2 location vectors that need to be used in the game logic for swapping
 */
USTRUCT(BlueprintType)
struct FSwapVectorPair
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FVector OldLocation;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FVector NewLocation;
	
};