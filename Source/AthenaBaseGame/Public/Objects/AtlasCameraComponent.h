// Any content from AthenaBaseGame is not to be reproduced or copied without permission from the owning author, redistribution is strictly proprietary, prohibited and confidential

#pragma once

#include "CoreMinimal.h"
#include "Camera/CameraComponent.h"
#include "AtlasCameraComponent.generated.h"

/**
 * 
 */
UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class ATHENABASEGAME_API UAtlasCameraComponent : public UCameraComponent
{
	GENERATED_BODY()

public:
	
	UFUNCTION(BlueprintCallable)
		void AddFieldOfViewOffset(float Offset);
};
