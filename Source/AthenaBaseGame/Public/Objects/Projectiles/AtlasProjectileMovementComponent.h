// Any content from AthenaBaseGame is not to be reproduced or copied without permission from the owning author, redistribution is strictly proprietary, prohibited and confidential

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "AtlasProjectileMovementComponent.generated.h"

/**
 * 
 */
UCLASS()
class ATHENABASEGAME_API UAtlasProjectileMovementComponent : public UProjectileMovementComponent
{
	GENERATED_BODY()

		virtual void StopSimulating(const FHitResult& HitResult);
	
};
