// Any content from AthenaBaseGame is not to be reproduced or copied without permission from the owning author, redistribution is strictly proprietary, prohibited and confidential

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Weapon_Atlas_Base.h"
#include "Components/BoxComponent.h"
#include "PSProjectile_Atlas.generated.h"

UCLASS()
class ATHENABASEGAME_API APSProjectile_Atlas : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APSProjectile_Atlas();

	UPROPERTY(EditDefaultsOnly)
		UBoxComponent* ProjectileCollision;

	UPROPERTY(EditDefaultsOnly)
		class UProjectileMovementComponent* ProjectileMovement;

	UPROPERTY(EditDefaultsOnly)
		UStaticMeshComponent* ProjectileMesh;

	UPROPERTY(EditDefaultsOnly)
		UParticleSystemComponent* ProjectileParticles;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Replicated)
		float ProjectileSpeed;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Replicated)
		float GravityDelay;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Replicated)
		float GravityScale;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Replicated)
		AWeapon_Atlas_Base* InstigatingWeapon;

	UFUNCTION()
		void OnProjectileHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);

	UFUNCTION()
		void ApplyGravity();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
