// Any content from AthenaBaseGame is not to be reproduced or copied without permission from the owning author, redistribution is strictly proprietary, prohibited and confidential

#pragma once

#include "CoreMinimal.h"
#include "Components/AudioComponent.h"
#include "Atlas_WeaponAudioComponent.generated.h"

/**
 * 
 */
UCLASS()
class ATHENABASEGAME_API UAtlas_WeaponAudioComponent : public UAudioComponent
{
	GENERATED_BODY()
	
};
