// Any content from AthenaBaseGame is not to be reproduced or copied without permission from the owning author, redistribution is strictly proprietary, prohibited and confidential

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "HeliosPlaylistManager.generated.h"

/**
 * 
 */
UCLASS()
class ATHENABASEGAME_API UHeliosPlaylistManager : public UObject
{
	GENERATED_BODY()
	
};
