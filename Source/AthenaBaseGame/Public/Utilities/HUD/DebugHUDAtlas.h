// Any content from AthenaBaseGame is not to be reproduced or copied without permission from the owning author, redistribution is strictly proprietary, prohibited and confidential

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "Engine/NetDriver.h"
#include "Misc/NetworkGuid.h"
#include "Engine/PackageMapClient.h"
#include "Engine/DemoNetDriver.h"
#include "DebugHUDAtlas.generated.h"

/**
 * 
 */
UCLASS()
class ATHENABASEGAME_API ADebugHUDAtlas : public AHUD
{
	GENERATED_BODY()
	
	//Gets the FNetworkGUID of a given actor as an FString
	UFUNCTION(BlueprintCallable, Category=NetHelpers, BlueprintPure)
	FString GetNetGUIDForActorAsString(AActor* Actor);
	
};
