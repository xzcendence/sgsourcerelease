// Any content from AthenaBaseGame is not to be reproduced or copied without permission from the owning author, redistribution is strictly proprietary, prohibited and confidential

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "DrawDebugHelpers.h"
#include "Engine/NetDriver.h"
#include "Misc/NetworkGuid.h"
#include "Engine/DemoNetDriver.h"
#include "HelpfulBlueprintFunctions.generated.h"

/**
 * 
 */
UCLASS()
class ATHENABASEGAME_API UHelpfulBlueprintFunctions : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

	UFUNCTION(BlueprintCallable, meta = (WorldContext = "WorldContextObject"))
		static void DrawDebugStringAtLocation(UObject* WorldContextObject, const FVector TextLocation, const FString& Text, class AActor* TestBaseActor = NULL, FLinearColor TextColor = FLinearColor::White, float Duration = 0.f, bool bDrawShadow = true, float FontScale = 1.f);
	UFUNCTION(BlueprintCallable)
		static void PrintStringToLog(bool bLogVerbose, FString LogString);
	UFUNCTION(BlueprintCallable, BlueprintPure)
		static bool IsInEditor();
};
