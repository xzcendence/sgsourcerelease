// Any content from AthenaBaseGame is not to be reproduced or copied without permission from the owning author, redistribution is strictly proprietary, prohibited and confidential

#pragma once

#include "CoreMinimal.h"
#include "Actors/Gameplay/Weapons/Weapon_Atlas_Base.h"
#include "AtlasScopedRifle.generated.h"

/**
 * 
 */
UCLASS()
class ATHENABASEGAME_API AAtlasScopedRifle : public AWeapon_Atlas_Base
{
	GENERATED_BODY()
	
};
