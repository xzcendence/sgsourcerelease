// Any content from AthenaBaseGame is not to be reproduced or copied without permission from the owning author, redistribution is strictly proprietary, prohibited and confidential

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "AtlasTypes.h"
#include "SwapPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class ASwapPlayerController : public APlayerController
{
	GENERATED_BODY()

public:

	ASwapPlayerController();

	UFUNCTION(BlueprintNativeEvent)
	void EnableCheatsDetected();


	UFUNCTION(BlueprintNativeEvent)
	void AddKillFeedEntry(FKillFeedEntryInfo KillFeedEntryInfo);
	
	UFUNCTION()
	virtual FLinearColor GetPlayerColor() const;
	
private:
	
	virtual void EnableCheats() override;
};
