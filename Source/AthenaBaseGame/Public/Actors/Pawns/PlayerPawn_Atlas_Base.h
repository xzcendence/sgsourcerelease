// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Components/InputComponent.h"
#include "GameFramework/Controller.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "AtlasCameraComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "DrawDebugHelpers.h"
#include "HelpfulBlueprintFunctions.h"
#include "Misc/NetworkGuid.h"
#include "Weapon_Atlas_Base.h"
#include "PlayerPawn_Atlas_Base.generated.h"

class AWeapon_Atlas_Base;

UCLASS()
class ATHENABASEGAME_API APlayerPawn_Atlas_Base : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	APlayerPawn_Atlas_Base();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		USpringArmComponent* CameraBoom;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		UAtlasCameraComponent* CameraM;

	void MoveForward(float Axis);
	void MoveRight(float Axis);
	void StartCrouching();
	void StopCrouching();

	UPROPERTY(BlueprintReadWrite, ReplicatedUsing=OnRep_NearbyWeapon)
		AWeapon_Atlas_Base* NearbyWeapon;

	UPROPERTY(BlueprintReadWrite, Replicated)
		AWeapon_Atlas_Base* EquippedWeapon;

	UPROPERTY(BlueprintReadWrite, Replicated)
		AWeapon_Atlas_Base* LastEquippedWeapon;

	//Our main color of the pawn
	UPROPERTY(Replicated, BlueprintReadWrite)
		FLinearColor PlayerMainColor;

	UPROPERTY(BlueprintReadWrite, ReplicatedUsing=OnRep_bIsJumpDown)
		bool bIsJumpDown;
	
	UFUNCTION(BlueprintNativeEvent)
		void OnRep_bIsJumpDown();

	UFUNCTION(BlueprintImplementableEvent)
		void OnRep_NearbyWeapon();

	UFUNCTION(BlueprintNativeEvent, BlueprintAuthorityOnly)
		void KillMinimalWorldOnly();

	UFUNCTION(BlueprintNativeEvent)
		void WeaponNearby(AWeapon_Atlas_Base* Weapon, bool ComingOrLeaving);

	UPROPERTY(BlueprintReadWrite, Replicated)
		bool bIsADSDown;

	UPROPERTY(BlueprintReadWrite, Replicated)
		int32 MainAmmo;

	UPROPERTY(BlueprintReadWrite, Replicated)
		int32 LauncherAmmo;

	UPROPERTY(BlueprintReadWrite)
		int32 CurrentAmmo;
	
	UPROPERTY(BlueprintReadWrite, Replicated)
		FHitResult WeaponTraceResult;

	UPROPERTY(BlueprintReadWrite, Replicated)
		TSubclassOf<AWeapon_Atlas_Base> EquippedWeaponClass;
	//Gets the world object
	UFUNCTION(BlueprintCallable, BlueprintPure)
		UWorld* GetWorldObject();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	
	virtual void FellOutOfWorld(const class UDamageType& dmgType) override;

};

