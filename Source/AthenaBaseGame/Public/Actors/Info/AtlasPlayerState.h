// Any content from AthenaBaseGame is not to be reproduced or copied without permission from the owning author, redistribution is strictly proprietary, prohibited and confidential

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerState.h"
#include "AtlasSwapGameState.h"
#include "Delegates/Delegate.h"
#include "AtlasPlayerState.generated.h"

/**
 * 
 */

USTRUCT(BlueprintType)
struct FPlayerMatchStatistic
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int32 SwapKills;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int32 Deaths;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int32 TotalMatchPoints;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int32 SwapPoints;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float DamageDealt;

	FPlayerMatchStatistic()
	{
		SwapKills = 0;
		SwapPoints = 0;
		Deaths = 0;
		TotalMatchPoints = 0;
		DamageDealt = 0.0f;
	}
};

UCLASS()
class ATHENABASEGAME_API AAtlasPlayerState : public APlayerState
{
	GENERATED_BODY()

public: 

	UPROPERTY(BlueprintReadWrite, Replicated)
		FPlayerMatchStatistic PlayerMatchStat;
};
