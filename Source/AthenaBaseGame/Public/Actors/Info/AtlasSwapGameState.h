// Any content from AthenaBaseGame is not to be reproduced or copied without permission from the owning author, redistribution is strictly proprietary, prohibited and confidential

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameState.h"
#include "Delegates/Delegate.h"
#include "SwapPlayerController.h"
#include "AtlasTypes.h"
#include "AtlasSwapGameState.generated.h"

class AWeapon_Atlas_Base;
class APlayerPawn_Atlas_Base;
class APlayerState;


/**
 * 
 */
UCLASS()
class ATHENABASEGAME_API AAtlasSwapGameState : public AGameState
{
	GENERATED_BODY()


public:
	
	AAtlasSwapGameState();
	
	DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FKillFeedEventDelegate, FKillFeedEntryInfo, KillFeedEntryInfo);

	/**
	 * Dynamic Multicast Delegate that fires whenever a player is killed, and contains the pertinent info about the kill
	 * @param KillFeedEntryInfo Pertinent info on the kill
	 */
	UPROPERTY(BlueprintAssignable, BlueprintAuthorityOnly, BlueprintCallable)
		FKillFeedEventDelegate OnKillFeedEvent;
	
	UFUNCTION(NetMulticast, Unreliable)
		void MulticastOnKillFeedEvent(FKillFeedEntryInfo KillFeedEntryInfo);

};
