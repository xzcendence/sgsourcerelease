// Any content from AthenaBaseGame is not to be reproduced or copied without permission from the owning author, redistribution is strictly proprietary, prohibited and confidential

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Info.h"
#include "AtlasWorldManager.generated.h"

/**
 * 
 */
UCLASS()
class ATHENABASEGAME_API AAtlasWorldManager : public AInfo
{
	GENERATED_BODY()
	
};
