// Any content from AthenaBaseGame is not to be reproduced or copied without permission from the owning author, redistribution is strictly proprietary, prohibited and confidential

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameMode.h"
#include "Kismet/GameplayStatics.h"
#include "PlayerPawn_Atlas_Base.h"
#include "AtlasSwapGameMode.generated.h"

class AWeapon_Atlas_Base;
class APlayerState;

/** 
 *  
 */  
UENUM(BlueprintType)
enum class ESwapType : uint8
{
	ST_Outbound		UMETA(DisplayName = "Outbound Swap"),
	ST_Inbound		UMETA(DisplayName = "Inbound Swap")
};


UCLASS()
class ATHENABASEGAME_API AAtlasSwapGameMode : public AGameMode
{
	GENERATED_BODY()

	UFUNCTION(BlueprintCallable)
		bool SetEnableInfiniteAmmoAllWeapons(const bool NewEnable);

	UFUNCTION(BlueprintCallable)
		void RefreshWorldWeaponPool();

	UFUNCTION(BlueprintCallable)
		void SetCanDealDamageAllWeapons(const bool NewEnable);

	UFUNCTION(BlueprintCallable, BlueprintAuthorityOnly)
		void RestartPlayerAtlas(AController* NewPlayer, APawn* OldPlayerPawn);

	TArray<APawn*> SuspendedPawns;

public:
	UPROPERTY(BlueprintReadWrite)
		TArray<AWeapon_Atlas_Base*> WorldWeaponPool;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	virtual APawn* SpawnDefaultPawnAtTransform_Implementation(AController* NewPlayer, const FTransform& SpawnTransform) override;
};
