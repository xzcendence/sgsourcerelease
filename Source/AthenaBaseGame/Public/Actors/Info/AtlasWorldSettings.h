// Any content from AthenaBaseGame is not to be reproduced or copied without permission from the owning author, redistribution is strictly proprietary, prohibited and confidential

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/WorldSettings.h"
#include "AtlasWorldSettings.generated.h"

/**
 * 
 */



UENUM(BlueprintType)
enum class EWorldDamageMode : uint8
{
	WDM_Enabled		UMETA(DisplayName = "Enabled"),
	WDM_Disabled	UMETA(DisplayName = "Disabled"),
	WDM_Dynamic		UMETA(DisplayName = "Dynamic") //If it is dynamic, it will most likely be managed by the current WorldManager
};



UCLASS()
class ATHENABASEGAME_API AAtlasWorldSettings : public AWorldSettings
{
	GENERATED_BODY()

public:

	AAtlasWorldSettings();


	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = World)
	int32 AtlasMapIndex;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = World)
	EWorldDamageMode WorldDamageMode;

	UFUNCTION(BlueprintPure, BlueprintCallable, meta = (WorldContext = "WorldContextObject"))
	static AAtlasWorldSettings* GetAtlasWorldSettings(UObject* WorldContextObject);

protected:

	virtual void BeginPlay() override;

public:

	virtual void Tick(float DeltaTime) override;
	
};
