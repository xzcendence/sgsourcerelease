// Any content from AthenaBaseGame is not to be reproduced or copied without permission from the owning author, redistribution is strictly proprietary, prohibited and confidential

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Particles/ParticleSystemComponent.h"
#include "Components/SphereComponent.h"
#include "Components/CapsuleComponent.h"
#include "Atlas_Pickup_Master.generated.h"



UCLASS()
class ATHENABASEGAME_API AAtlas_Pickup_Master : public AActor
{
	GENERATED_BODY()

public:	
	// Sets default values for this actor's properties
	AAtlas_Pickup_Master();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		UParticleSystemComponent* ItemParticle;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		UCapsuleComponent* PickupBounds;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		int32 Value;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
