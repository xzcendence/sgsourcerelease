// Any content from AthenaBaseGame is not to be reproduced or copied without permission from the owning author, redistribution is strictly proprietary, prohibited and confidential

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/SkeletalMeshComponent.h"
#include "Components/SphereComponent.h"
#include "Engine/DataTable.h"
#include "Math/Color.h"
#include "GameFramework/Pawn.h"
//#include "AthenaBaseGame/Public/PlayerPawn_Atlas_Base.h"
#include "Weapon_Atlas_Base.generated.h"



class APlayerPawn_Atlas_Base;
class UTexture2D;

UENUM(BlueprintType)
enum class EWeaponRarity : uint8
{
	WR_Common		UMETA(DisplayName="Common"),
	WR_Uncommon		UMETA(DisplayName="Uncommon"),
	WR_Rare			UMETA(DisplayName="Rare"),
	WR_Ultra		UMETA(DisplayName="Ultra"),
	WR_Legendary	UMETA(DisplayName="Legendary")
};

UENUM(BlueprintType)
enum class EReticleType : uint8
{
	RT_Rifle		UMETA(DisplayName="Rifle"),
	RT_HML			UMETA(DisplayName="HomingMissileLauncher"),
	RT_Sniper		UMETA(DisplayName="Sniper")
};

UENUM(BlueprintType)
enum class EAmmoType : uint8
{
	AT_Rifle		UMETA(DisplayName="5.56"),
	AT_Rocket		UMETA(DisplayName="Rockets")
};

USTRUCT(BlueprintType)
struct FWeaponAttributes : public FTableRowBase
{
	GENERATED_BODY()
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TSubclassOf<AWeapon_Atlas_Base> WeaponClass;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float WeaponRange;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FString FriendlyName;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool bIsScoped;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		EWeaponRarity Rarity;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UTexture2D* PreviewImage;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FColor RarityColor;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float WeaponDamage;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int32 HeadshotMultiplier;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float ReloadTimeInSeconds;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int32 MaxClipAmmo;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float FireRate;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int32 ReticleBloomShiftOffsetMultiplier;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool bUsePersistentTraceForTrajectory;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		EReticleType ReticleType;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		EAmmoType AmmoType;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool bIsAutomatic;
	
	FWeaponAttributes()
	{
		WeaponRange = 0.0;
		WeaponDamage = 1.0f;
		Rarity = EWeaponRarity::WR_Common;
		ReticleType = EReticleType::RT_Rifle;
		RarityColor = FColor::Red;
		AmmoType = EAmmoType::AT_Rifle;
		PreviewImage = UTexture2D::CreateTransient(32, 32, PF_B8G8R8A8);	//Create a blank transient texture just for initialization purposes
		FriendlyName = FString("EmptyWeaponFriendlyName");
		HeadshotMultiplier = 1;
		ReloadTimeInSeconds = 0.1;
		MaxClipAmmo = 1;
		FireRate = 0.1;
		bIsScoped = false;
		ReticleBloomShiftOffsetMultiplier = 0;
		bUsePersistentTraceForTrajectory = true;
		bIsAutomatic = false;
	}
};

UCLASS()
class ATHENABASEGAME_API AWeapon_Atlas_Base : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AWeapon_Atlas_Base();

	UFUNCTION(BlueprintNativeEvent)
		void OnPlayerEnterPickupBounds(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION(BlueprintNativeEvent)
		void OnPlayerLeavePickupBounds(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	UFUNCTION(BlueprintCallable, Server, Reliable, WithValidation)
		void SRVFire();

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
		void Fire();

	UFUNCTION(BlueprintCallable, Server, Reliable, WithValidation)
		void SRVFireRelease();

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
		void FireRelease();

	UFUNCTION(BlueprintNativeEvent)
		void OnProjectileHit(const FVector HitLocation, AActor* HitActor, APawn* InstigatingPawn);

	UFUNCTION(BlueprintCallable)
		void DetachFromOwner();

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
		void Equip();

	UFUNCTION(BlueprintCallable, BlueprintAuthorityOnly)
		bool RegisterToWorldWeaponPool();

	UFUNCTION(BlueprintCallable, BlueprintAuthorityOnly)
		bool DeregisterFromWorldWeaponPool();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		USkeletalMeshComponent* WeaponMesh;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		USphereComponent* PickupBounds;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		USphereComponent* MuzzleLocation;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Replicated)
		FWeaponAttributes WeaponAttributes;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		EWeaponRarity WeaponRarityBase;

	UPROPERTY(BlueprintReadWrite, Replicated)
		bool bCanDealDamage;

	UPROPERTY(BlueprintReadWrite, Replicated)
		bool bHasInfiniteAmmo;
	
	UPROPERTY(BlueprintReadWrite, Replicated)
		bool bCanBePickedUp;

	UPROPERTY(BlueprintReadWrite, Replicated)
		int32 CurrentClipAmmo;

	UPROPERTY(BlueprintReadWrite)
		bool bIsEquipped;

	UPROPERTY(BlueprintReadWrite, Replicated)
		bool bIsInUse;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
		FTransform PreferredRelAttachmentTransform;

	UPROPERTY(BlueprintReadWrite, Replicated)
		APlayerPawn_Atlas_Base* GunmanOwner;

	UPROPERTY(BlueprintReadWrite, Replicated)
		FGuid WeaponGUID;

	void AssignWeaponGUID();
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	virtual void Destroyed() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};



