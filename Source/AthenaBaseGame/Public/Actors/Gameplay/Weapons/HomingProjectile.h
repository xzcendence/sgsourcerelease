// Any content from AthenaBaseGame is not to be reproduced or copied without permission from the owning author, redistribution is strictly proprietary, prohibited and confidential

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "AtlasProjectileMovementComponent.h"
#include "Particles/ParticleSystemComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/AudioComponent.h"
#include "Weapon_Atlas_Base.h"
#include "Kismet/GameplayStatics.h"
#include "Sound/SoundBase.h"
#include "Sound/SoundAttenuation.h"
#include "HomingProjectile.generated.h"

//class UAtlasProjectileMovementComponent;

UCLASS()
class ATHENABASEGAME_API AHomingProjectile : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AHomingProjectile();

	UPROPERTY(EditDefaultsOnly)
		UAtlasProjectileMovementComponent* ProjectileMovement;

	UPROPERTY(EditDefaultsOnly, Category=ComponentDefaults)
		UStaticMeshComponent* ProjectileMesh;

	UPROPERTY(EditDefaultsOnly, Category=ComponentDefaults)
		UCapsuleComponent* ProjectileCollision;

	UPROPERTY(EditDefaultsOnly, Category=ComponentDefaults)
		UParticleSystemComponent* HeadingParticle;

	UPROPERTY(EditDefaultsOnly, Category=ComponentDefaults)
		UParticleSystemComponent* TrailParticle;

	UPROPERTY(EditDefaultsOnly, Category=ComponentDefaults)
		UAudioComponent* InAirSouund;

	UPROPERTY(BlueprintReadWrite, Replicated, meta = (ExposeOnSpawn = "true"))
		AWeapon_Atlas_Base* InstigatingWeapon;
	
	UPROPERTY(BlueprintReadWrite, Replicated)
		USceneComponent* HomingTarget;

	UPROPERTY(EditDefaultsOnly, Category=EffectDefaults)
		USoundBase* ProjectileImpactSound;

	UPROPERTY(EditDefaultsOnly, Category=EffectDefaults)
		USoundAttenuation* ProjectileImpactSoundAttenuation;

	UPROPERTY(EditDefaultsOnly, Category=EffectDefaults)
		UParticleSystem* ProjectileImpactParticle;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=VariableDefaults, meta = (ExposeOnSpawn = "true"))
		float HomingAccelerationMagnitude;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=VariableDefaults, meta = (ExposeOnSpawn = "true"))
		float MaxSpeed;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=VariableDefaults, meta = (ExposeOnSpawn = "true"))
		float InitialSpeed;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = VariableDefaults, meta = (ExposeOnSpawn = "true"))
		float HomingLifetime;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = VariableDefaults, meta = (ExposeOnSpawn = "true"))
		float FullLifetime;



	UFUNCTION()
		void OnProjectileBeginOverlap(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);

	UFUNCTION(BlueprintCallable)
		void SetHomingTarget(USceneComponent* HomingTargetComp, float DelayToStartHoming);

	UFUNCTION()
		void SetVelocityForHomingDelay();

	UFUNCTION(BlueprintCallable)
		void ForceExplode();


	UFUNCTION(NetMulticast, Reliable)
		void NetMulticast_ProjectileHitEffects(const FVector HitLocation);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
