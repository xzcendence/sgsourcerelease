// Any content from AthenaBaseGame is not to be reproduced or copied without permission from the owning author, redistribution is strictly proprietary, prohibited and confidential

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "GameFramework/Character.h"
#include "Sound/SoundBase.h"
#include "Particles/ParticleSystemComponent.h"
#include "JumpPad_Atlas.generated.h"

UCLASS()
class ATHENABASEGAME_API AJumpPad_Atlas : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AJumpPad_Atlas();

	UPROPERTY(EditAnywhere)
		USoundBase* LaunchSound;

	UPROPERTY(EditAnywhere)
		int32 LaunchVelocityMultiplier;

	UPROPERTY(EditDefaultsOnly)
		UStaticMeshComponent* StaticMesh;

	UPROPERTY(EditAnywhere)
		UArrowComponent* ArrowPoint;

	UPROPERTY(EditAnywhere)
		UParticleSystemComponent* DirectionPointParticle;

	UFUNCTION(NetMulticast, Reliable)
		void LaunchCharacter_Multi(ACharacter* LaunchTarget, FVector LaunchVelocity);

	UFUNCTION()
		void OnStaticMeshOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
