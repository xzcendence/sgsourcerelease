// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "AthenaBaseGame.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, AthenaBaseGame, "AthenaBaseGame" );
DEFINE_LOG_CATEGORY(LogAtlas);
DEFINE_LOG_CATEGORY(LogAtlasVerbose);
